﻿using System.Threading.Tasks;
using Abp.Application.Services;
using applied.Test.Authorization.Accounts.Dto;

namespace applied.Test.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}

﻿using AutoMapper;
using applied.Test.Employees.Dto;
using applied.Test.Company.Dto;

namespace applied.Test
{
    public class AutoMapperConfigurationProfile : Profile
    {
        public AutoMapperConfigurationProfile()
        {
            CreateMap<CreateEmployeesDto, Employee.Employees>();
            CreateMap<Employee.Employees, EmployeesDto>();

            CreateMap<CreateCompanyDto, Employee.Company>();
            CreateMap<Employee.Company, CompanyDto>();
        }
    }
}

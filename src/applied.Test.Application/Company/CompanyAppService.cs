﻿using Abp.Domain.Repositories;
using applied.Test.Company.Dto;
using System;
using System.Linq;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using applied.Test.Employee;
using applied.Test.MultiTenancy.Dto;
using applied.Test.MultiTenancy;
using Abp.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using applied.Test.Employees;
using applied.Test.Employees.Dto;


namespace applied.Test.Company
{
    public class CompanyAppService : TestAppServiceBase, ICompanyAppService
    {
        private readonly IRepository<Employee.Company, Guid> _CompanyRepository;

        public CompanyAppService(
         IRepository<Employee.Company, Guid> companyRepository)

        {
            _CompanyRepository = companyRepository;
        }

        public async Task<CompanyDto> CreateAsync(CreateCompanyDto input)
        {
            var company = ObjectMapper.Map<Employee.Company>(input);
            var result = await _CompanyRepository.InsertAsync(company);
            var resultDto = ObjectMapper.Map<CompanyDto>(result);

            return resultDto;

        }

        public async Task<CompanyDto> UpdateAsync(CompanyDto input)
        {
            // Fetch the employee entity from the repository
            var employee = await _CompanyRepository.GetAsync(input.Id);

            // Update the entity properties
            employee.Name = input.Name;
            employee.Description = input.Description;
            employee.IsActived = input.IsActived;

            // Save the updated entity
            await _CompanyRepository.UpdateAsync(employee);

            // Map the updated entity back to a DTO
            var resultDto = ObjectMapper.Map<CompanyDto>(employee);

            return resultDto;
        }

        public async Task<List<CompanyDto>> GetCompanyList()
        {
            // Fetch the list of employees from the repository
            var company = await _CompanyRepository.GetAllListAsync();
            var companyDtos = ObjectMapper.Map<List<CompanyDto>>(company);

            return companyDtos;
        }

        public async Task DeleteAsync(Guid id)
        {
            //CheckDeletePermission();

            var tenant = await _CompanyRepository.GetAsync(id);
            await _CompanyRepository.DeleteAsync(tenant);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace applied.Test.Company.Dto
{
    public class CreateCompanyDto
    {
        public string Name { get; set; }

        public string Description { get; set; }
        public string IsActived { get; set; }
    }
}


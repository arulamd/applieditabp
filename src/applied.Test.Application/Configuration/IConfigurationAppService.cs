﻿using System.Threading.Tasks;
using applied.Test.Configuration.Dto;

namespace applied.Test.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}

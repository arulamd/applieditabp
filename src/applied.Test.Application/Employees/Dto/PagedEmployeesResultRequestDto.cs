﻿using Abp.Application.Services.Dto;

namespace applied.Test.Employees.Dto
{
    public class PagedEmployeesResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
        public bool? IsActive { get; set; }
    
    }
}

﻿

using Abp.Domain.Repositories;
using applied.Test.Employees.Dto;
using System;
using System.Linq;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using applied.Test.Employee;
using applied.Test.MultiTenancy.Dto;
using applied.Test.MultiTenancy;
using Abp.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.RateLimiting;
using Abp.Authorization;
using applied.Test.Authorization;

namespace applied.Test.Employees
{
    [AbpAuthorize(PermissionNames.Pages_Employees)]
    [EnableRateLimiting("fixed-by-user2")]
    public class EmployeesAppService : TestAppServiceBase, IEmployeesAppService
    {
        private readonly IRepository<Employee.Employees, Guid> _employeesRepository;

        public EmployeesAppService(
          IRepository<Employee.Employees, Guid> employeesRepository)

        {
            _employeesRepository = employeesRepository;
        }

        public async Task<EmployeesDto> CreateAsync(CreateEmployeesDto input)
        {
            var Employee = ObjectMapper.Map<Employee.Employees>(input);
            var result = await _employeesRepository.InsertAsync(Employee);
            var resultDto = ObjectMapper.Map<EmployeesDto>(result);

            return resultDto;

        }

        public async Task<EmployeesDto> UpdateAsync(EmployeesDto input)
        {
            // Fetch the employee entity from the repository
            var employee = await _employeesRepository.GetAsync(input.Id);

            // Update the entity properties
            employee.Name = input.Name;
            employee.Description = input.Description;
            employee.IsActived = input.IsActived;

            // Save the updated entity
            await _employeesRepository.UpdateAsync(employee);

            // Map the updated entity back to a DTO
            var resultDto = ObjectMapper.Map<EmployeesDto>(employee);

            return resultDto;
        }

        [HttpGet]        
        public async Task<List<EmployeesDto>> GetEmployeeList()
        {
            // Fetch the list of employees from the repository
            var employees = _employeesRepository.GetAll().Include(x=> x.Company);           
            var employeeDtos = ObjectMapper.Map<List<EmployeesDto>>(employees);
                        
            return employeeDtos;
        }

       
        public async Task<EmployeesDto> GetEmployeeById(Guid id)
        {
            // Fetch the employee by id from the repository
            var employee = await _employeesRepository.FirstOrDefaultAsync(x=>x.Id==id);
            var employeeDtos = ObjectMapper.Map<EmployeesDto>(employee);

            return employeeDtos;

        }

        public async Task DeleteAsync(Guid id)
        {
            //CheckDeletePermission();

            var tenant = await _employeesRepository.GetAsync(id);
            await _employeesRepository.DeleteAsync(tenant);
        }

    }
}

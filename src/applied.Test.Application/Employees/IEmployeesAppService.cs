﻿using Abp.Application.Services;
using applied.Test.Employees.Dto;
using applied.Test.MultiTenancy.Dto;

namespace applied.Test.Employees
{
    public interface IEmployeesAppService : IApplicationService
    {
    }

}

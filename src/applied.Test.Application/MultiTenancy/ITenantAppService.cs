﻿using Abp.Application.Services;
using applied.Test.MultiTenancy.Dto;

namespace applied.Test.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}


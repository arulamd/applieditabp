﻿using System.Threading.Tasks;
using Abp.Application.Services;
using applied.Test.Sessions.Dto;

namespace applied.Test.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}

﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using applied.Test.Authorization;
using AutoMapper;


namespace applied.Test
{
    [DependsOn(
        typeof(TestCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class TestApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<TestAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(TestApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );

            var config = new MapperConfiguration(cfg =>
            {

                cfg.AddProfile(typeof(AutoMapperConfigurationProfile));
                //cfg.ForAllPropertyMaps(map =>
                //        map.SourceMember.GetCustomAttributes().OfType<ReadOnlyAttribute>().Any(x => x.IsReadOnly),
                //    (map, configuration) => { configuration.Ignore(); });
            });
            AutoMapperConfiguration.Init(config);

        }
      

    }
}

using System.ComponentModel.DataAnnotations;

namespace applied.Test.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
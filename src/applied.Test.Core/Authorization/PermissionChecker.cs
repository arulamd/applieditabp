﻿using Abp.Authorization;
using applied.Test.Authorization.Roles;
using applied.Test.Authorization.Users;

namespace applied.Test.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}

﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace applied.Test.Employee
{
    [Table("Employees")]
    public class Employees : AuditedEntity<Guid>
    {
        public const int MaxTitleLength = 256;
        public const int MaxDescriptionLength = 64 * 1024; //64KB

        [Required]
        [StringLength(MaxTitleLength)]
        public string Name { get; set; }

        [StringLength(MaxDescriptionLength)]
        public string Description { get; set; }
        public string IsActived { get; set; }

        [Required]
        [ForeignKey("Company")]
        public Guid CompanyId { get; set; }
        public virtual Employee.Company Company { get; set; }


    }

    [Table("Company")]
    public class Company : AuditedEntity<Guid>
    {         
        public string Name { get; set; }
        public string Description { get; set; }
        public string IsActived { get; set; }        
       

    }
}

﻿using applied.Test.Debugging;

namespace applied.Test
{
    public class TestConsts
    {
        public const string LocalizationSourceName = "Test";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;


        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public static readonly string DefaultPassPhrase =
            DebugHelper.IsDebug ? "gsKxGZ012HLL3MI5" : "64d92b1b2f554275b70e583ec977d0fe";
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using applied.Test.Authorization.Roles;
using applied.Test.Authorization.Users;
using applied.Test.MultiTenancy;
using applied.Test.Employee;

namespace applied.Test.EntityFrameworkCore
{
    public class TestDbContext : AbpZeroDbContext<Tenant, Role, User, TestDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Company> Company { get; set; }

        public TestDbContext(DbContextOptions<TestDbContext> options)
            : base(options)
        {
        }
    }
}

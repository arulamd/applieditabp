﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using applied.Test.Controllers;

namespace applied.Test.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : TestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}

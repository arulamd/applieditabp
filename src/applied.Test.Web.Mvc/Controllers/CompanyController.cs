﻿using Microsoft.AspNetCore.Mvc;
using applied.Test.Controllers;
using applied.Test.Company;
using applied.Test.Company.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using applied.Test.Employees.Dto;
using System;
using applied.Test.Employee;

namespace applied.Test.Web.Controllers
{
    public class CompanyController : TestControllerBase
    {
        private readonly CompanyAppService _companyAppService;

        public CompanyController(CompanyAppService companyAppService)
        {
            _companyAppService = companyAppService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> saveCompany(CreateCompanyDto dto)
        {
            await _companyAppService.CreateAsync(dto);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UpdateCompany(CompanyDto dto)
        {

            await _companyAppService.UpdateAsync(dto);
            return RedirectToAction("Index");
        }


        [HttpGet]
        public async Task<IActionResult> GetCompany()
        {
            var employees = await _companyAppService.GetCompanyList();

            var list = new List<object> { employees.Select(x => new { x.Id, x.Description, x.Name, x.IsActived }) };

            var result = new
            {
                draw = 1, // This should ideally come from the request for server-side processing
                recordsTotal = employees.Count,
                recordsFiltered = employees.Count,
                data = employees.Select(x => x).ToList().ToArray()
            };

            return Json(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCompanies()
        {
            var companies = await _companyAppService.GetCompanyList();
            var data = companies.Select(x => x).ToList().ToArray();
            return Json(data);
        }

        [HttpGet]
        public async Task<ActionResult> EditModal(Guid companyId)
        {
            var employees = await _companyAppService.GetCompanyList();

            var data = employees.Select(x => new { x.Id, x.Description, x.Name, x.IsActived }).Where(x => x.Id == companyId).FirstOrDefault();

            if (data != null)
            {
                var viewModel = new CompanyDto()
                {
                    Id = data.Id,
                    Name = data.Name,
                    Description = data.Description,
                };

                return PartialView("_EditModal", viewModel);
            }

            return BadRequest();
        }

        [HttpGet]
        public async Task<ActionResult> DeleteCompany(Guid employeeId)
        {
            await _companyAppService.DeleteAsync(employeeId);

            return RedirectToAction("Index");
        }

    }
}

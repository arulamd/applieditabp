﻿using Abp.AspNetCore.Mvc.Authorization;
using applied.Test.Authorization;
using applied.Test.Controllers;
using applied.Test.Employees;
using applied.Test.Company;
using applied.Test.Employees.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using System;
using applied.Test.Employee;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.RateLimiting;


namespace applied.Test.Web.Controllers
{
   [AbpMvcAuthorize(PermissionNames.Pages_Employees)]
   
    public class EmployeesController : TestControllerBase
    {
        private readonly EmployeesAppService _employeesAppService;
        private readonly CompanyAppService _companyAppService;

        public EmployeesController(EmployeesAppService employeesAppService, CompanyAppService companyAppService)
        {
            _employeesAppService = employeesAppService;
            _companyAppService = companyAppService;
        }

        public IActionResult Index()
        {           
            return View();
        }

        public async Task<IActionResult> saveEmployee(CreateEmployeesDto dto)
        {
            await _employeesAppService.CreateAsync(dto);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UpdateEmployee(EmployeesDto dto)
        {
            
            await _employeesAppService.UpdateAsync(dto);
            return RedirectToAction("Index");
        }
        

        [HttpGet]
        //[EnableRateLimiting("fixed-by-ip")]
        public async Task<IActionResult> GetEmployee()
        {
            var employees = await _employeesAppService.GetEmployeeList();

            var list = new List<object> { employees.Select(x => new { x.Id, x.Description, x.Name, x.IsActived }) };

            var result = new
            {
                draw = 1, // This should ideally come from the request for server-side processing
                recordsTotal = employees.Count,
                recordsFiltered = employees.Count,
                data = employees.Select(x => x).ToList().ToArray()
            };

            return Json(result);
        }

        [HttpGet]
        public async Task<ActionResult> EditModal(Guid employeeId)
        {
            var employees = await _employeesAppService.GetEmployeeList();

            var data = employees.Select(x => new { x.Id, x.Description, x.Name, x.IsActived }).Where(x => x.Id == employeeId).FirstOrDefault();

            if (data != null)
            {
                var viewModel = new EmployeesDto()
                {
                    Id = data.Id,
                    Name = data.Name,
                    Description = data.Description,
                };

                return PartialView("_EditModal", viewModel);
            }

            return BadRequest();
        }

        [HttpGet]
        public async Task<ActionResult> DeleteEmployee(Guid employeeId)
        {
           await _employeesAppService.DeleteAsync(employeeId);

           return RedirectToAction("Index");
        }


    }
}

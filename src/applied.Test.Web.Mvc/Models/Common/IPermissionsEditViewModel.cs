﻿using System.Collections.Generic;
using applied.Test.Roles.Dto;

namespace applied.Test.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}
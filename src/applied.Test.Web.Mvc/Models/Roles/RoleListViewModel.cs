﻿using System.Collections.Generic;
using applied.Test.Roles.Dto;

namespace applied.Test.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}

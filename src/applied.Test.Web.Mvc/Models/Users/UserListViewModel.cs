using System.Collections.Generic;
using applied.Test.Roles.Dto;

namespace applied.Test.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}

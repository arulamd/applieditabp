﻿namespace applied.Test.Web.Startup
{
    public class PageNames
    {
        public const string Home = "Home";
        public const string About = "About";
        public const string Tenants = "Tenants";
        public const string Employees = "Employees";
        public const string Company = "Company";
        public const string Users = "Users";
        public const string Roles = "Roles"; 
    }
}

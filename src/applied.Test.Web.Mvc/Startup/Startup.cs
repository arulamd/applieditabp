﻿using System;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Castle.Facilities.Logging;
using Abp.AspNetCore;
using Abp.AspNetCore.Mvc.Antiforgery;
using Abp.Castle.Logging.Log4Net;
using applied.Test.Authentication.JwtBearer;
using applied.Test.Configuration;
using applied.Test.Identity;
using applied.Test.Web.Resources;
using Abp.AspNetCore.SignalR.Hubs;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.WebEncoders;
using System.IO;
using AspNetCoreRateLimit;
using System.Collections.Generic;
using Microsoft.AspNetCore.RateLimiting;
using System.Threading.RateLimiting;
using applied.Test.Authorization.Users;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace applied.Test.Web.Startup
{
    public class Startup
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfigurationRoot _appConfiguration;

        public Startup(IWebHostEnvironment env)
        {
            _hostingEnvironment = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // MVC
            services.AddControllersWithViews(
                    options =>
                    {
                        options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                        options.Filters.Add(new AbpAutoValidateAntiforgeryTokenAttribute());
                    }
                )
                .AddRazorRuntimeCompilation();

            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);

            services.Configure<WebEncoderOptions>(options =>
            {
                options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.All);
            });         

            services.AddScoped<IWebResourceManager, WebResourceManager>();

            services.AddSignalR();

            // Configure Abp and Dependency Injection
            services.AddAbpWithoutCreatingServiceProvider<TestWebMvcModule>(
                // Configure Log4Net logging
                options => options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig(
                        _hostingEnvironment.IsDevelopment()
                            ? "log4net.config"
                            : "log4net.Production.config"
                        )
                )
            );

            //services.AddRateLimiter(options =>
            //{
            //    options.AddPolicy("fixed-by-ip", httpContext =>
            //        RateLimitPartition.GetFixedWindowLimiter(
            //            partitionKey: httpContext.Connection.RemoteIpAddress?.ToString(),
            //            factory: _ => new FixedWindowRateLimiterOptions
            //            {
            //                PermitLimit = 10,
            //                Window = TimeSpan.FromMinutes(1)
            //            }));
            //});

            services.AddRateLimiter(options =>
            {
                options.OnRejected = (context, cancellationToken) =>
                {
                    if (context.Lease.TryGetMetadata(MetadataName.RetryAfter, out var retryAfter))
                    {
                        context.HttpContext.Response.Headers.RetryAfter =
                            ((int)retryAfter.TotalSeconds).ToString(NumberFormatInfo.InvariantInfo);
                    }

                    context.HttpContext.Response.StatusCode = StatusCodes.Status429TooManyRequests;
                    context.HttpContext.RequestServices.GetService<ILoggerFactory>()?
                        .CreateLogger("Microsoft.AspNetCore.RateLimitingMiddleware")
                        .LogWarning("OnRejected: {RequestPath}", context.HttpContext.Request.Path);

                    return new ValueTask();
                };

                options.AddPolicy("fixed-by-ip", httpContext =>
                    RateLimitPartition.GetFixedWindowLimiter(
                    partitionKey: httpContext.Connection.RemoteIpAddress?.ToString(),
                    factory: _ => new FixedWindowRateLimiterOptions
                    {
                        PermitLimit = 10,
                        Window = TimeSpan.FromMinutes(1)
                    }));

                //options.AddPolicy("fixed-by-ip", httpContext =>
                //RateLimitPartition.GetFixedWindowLimiter(
                //    httpContext.Request.Headers["X-Forwarded-For"].ToString(),
                //    factory: _ => new FixedWindowRateLimiterOptions
                //    {
                //        PermitLimit = 10,
                //        Window = TimeSpan.FromMinutes(1)
                //    }));

                options.AddPolicy("fixed-by-user", httpContext =>
                RateLimitPartition.GetFixedWindowLimiter(
                partitionKey: httpContext.User.Identity?.Name?.ToString(),
                factory: _ => new FixedWindowRateLimiterOptions
                {
                    PermitLimit = 5,
                    Window = TimeSpan.FromMinutes(1)
                }));

                options.AddPolicy("fixed-by-user2", httpContext =>
                {
                    var userIdentifier = httpContext.User.FindFirst(JwtRegisteredClaimNames.Sub)?.Value;

                    return RateLimitPartition.GetFixedWindowLimiter(
                        partitionKey: userIdentifier,
                        factory: _ => new FixedWindowRateLimiterOptions
                        {
                            PermitLimit = 10,
                            Window = TimeSpan.FromMinutes(1)
                        });
                });
            });

            //services.AddMemoryCache();
            //services.Configure<IpRateLimitOptions>(options =>
            //{
            //    options.EnableEndpointRateLimiting = true;
            //    options.StackBlockedRequests = false;
            //    options.HttpStatusCode = 429;
            //    options.RealIpHeader = "X-Real-IP";
            //    options.ClientIdHeader = "X-ClientId";
            //    options.GeneralRules = new List<RateLimitRule>{

            //        //new RateLimitRule
            //        //{
            //        //    Endpoint = "*",
            //        //    Period = "20s",
            //        //    Limit = 5,
            //        //}
            //        new RateLimitRule
            //        {
            //            Endpoint = "/api/services/app/Employees/*",
            //            Period = "20s",
            //            Limit = 5,
            //        }
            //    };
            //});


            //services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            //services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            //services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            //services.AddSingleton<IProcessingStrategy, AsyncKeyLockProcessingStrategy>();
            //services.AddInMemoryRateLimiting();

            //services.AddRateLimiter(limiterOptions =>
            //{
            //    limiterOptions.OnRejected = (context, cancellationToken) =>
            //    {
            //        if (context.Lease.TryGetMetadata(MetadataName.RetryAfter, out var retryAfter))
            //        {
            //            context.HttpContext.Response.Headers.RetryAfter =
            //                ((int)retryAfter.TotalSeconds).ToString(NumberFormatInfo.InvariantInfo);
            //        }

            //        context.HttpContext.Response.StatusCode = StatusCodes.Status429TooManyRequests;
            //        context.HttpContext.RequestServices.GetService<ILoggerFactory>()?
            //            .CreateLogger("Microsoft.AspNetCore.RateLimitingMiddleware")
            //            .LogWarning("OnRejected: {RequestPath}", context.HttpContext.Request.Path);

            //        return new ValueTask();
            //    };

            //    limiterOptions.AddPolicy("UserBasedRateLimiting", context =>
            //    {
            //        //var currentUser = context.RequestServices.GetService<ICurrentUser>();

            //        if (context.RequestServices.GetService<User>() is User currentUser)
            //        {
            //            return RateLimitPartition.GetTokenBucketLimiter(User.AdminUserName.ToString(), _ => new TokenBucketRateLimiterOptions
            //            {
            //                TokenLimit = 10,
            //                QueueProcessingOrder = QueueProcessingOrder.OldestFirst,
            //                QueueLimit = 3,
            //                ReplenishmentPeriod = TimeSpan.FromMinutes(1),
            //                TokensPerPeriod = 4,
            //                AutoReplenishment = true
            //            });
            //        }

            //        return RateLimitPartition.GetSlidingWindowLimiter("anonymous-user",
            //            _ => new SlidingWindowRateLimiterOptions
            //            {
            //                PermitLimit = 2,
            //                QueueProcessingOrder = QueueProcessingOrder.OldestFirst,
            //                QueueLimit = 1,
            //                Window = TimeSpan.FromMinutes(1),
            //                SegmentsPerWindow = 2
            //            });
            //    });


            //});
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAbp(); // Initializes ABP framework.


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseRateLimiter();

            app.UseAuthentication();

            app.UseJwtTokenMiddleware();

            app.UseAuthorization();

            //app.UseIpRateLimiting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<AbpCommonHub>("/signalr");
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute("defaultWithArea", "{area}/{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

﻿(function ($) {
    //var testUrl = '/Employees/GetEmployee';
    //$.get(testUrl).done(function (testJson) {
    //    console.log(testJson)
    //})

    _$modal = $('#CompanyCreateModal'),
        _$form = _$modal.find('form'),
        _$table = $('#CompanyTable');

    var _$employeesTable = _$table.DataTable({
        processing: true,
        //serverSide: true,
        listAction: {
            inputFilter: function () {
                return $('#CompanySearchForm').serializeFormToObject(true);
            }
        },
        ajax: {
            "url": '/Company/GetCompany',
            "type": "GET",
            "contentType": "application/json",
            "datatype": "json",
            "dataSrc": function (data) {
                //console.log((data.result));
                var tableData = data.result;
                return tableData.data;
            }

        },
        buttons: [
            {
                name: 'refresh',
                text: '<i class="fas fa-redo-alt"></i>',
                action: () => _$employeesTable.draw(false)
            }
        ],
        responsive: {
            details: {
                type: 'column'
            }
        },
        columns: [
            { "data": "id" },
            { "data": "name" },
            { "data": "description" },
            { "data": "isActived" },
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    return [
                        `<button type="button" class="btn btn-sm bg-secondary edit-company" data-company-id="${row.id}" data-toggle="modal" data-target="#CompanyEditModal">`,
                        `<i class="fas fa-pencil-alt"></i> Edit`,
                        `</button>`,
                        `<button type="button" class="btn btn-sm bg-danger delete-company" data-company-id="${row.id}" data-company-name="${row.name}">`,
                        `    <i class="fas fa-trash"></i> Delete`,
                        `</button>`
                    ].join('');
                },
                "orderable": false,
                "searchable": false
            }
        ]
        //});
    });

    $(document).on('click', '.delete-company', function () {
        var tenantId = $(this).attr('data-company-id');
        var tenancyName = $(this).attr('data-company-name');

        deleteTenant(tenantId, tenancyName);
    });

    $(document).on('click', '.edit-company', function (e) {
        var tenantId = $(this).attr('data-company-id');

        $.ajax({
            url: '/Company/EditModal?companyId=' + tenantId,
            type: 'GET',
            dataType: 'html',
            success: function (content) {
                $('#CompanyEditModal div.modal-content').html(content);
            },
            error: function (e) {
            }
        });
    });
   

    abp.event.on('company.edited', (data) => {
        _$employeesTable.ajax.reload();
    });

    function deleteTenant(tenantId, tenancyName) {

        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this data? " + tenancyName,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(willDelete => {
                if (willDelete) {
                    $.ajax({
                        url: '/Company/DeleteCompany?companyId=' + tenantId,
                        type: 'GET',
                        contentType: 'application/json',
                        success: function (result) {
                            _$modal.modal('hide');
                            swal("Delete!", "Your Company is Delete!", "success");
                            //abp.event.trigger('employee.edited', tenancyName);
                            _$employeesTable.ajax.reload();
                        },
                        error: function (error) {
                            console.error("Update failed:", error);
                        }
                    });

                }
                else {
                    swal("Safe!", "Your file is safe!", "success");
                }
            });
    }

    _$modal.on('shown.bs.modal', () => {
        _$modal.find('input:not([type=hidden]):first').focus();
    }).on('hidden.bs.modal', () => {
        _$form.clearForm();
    });

    $('.btn-search').on('click', (e) => {
        _$employeesTable.ajax.reload();
    });

    $('.btn-clear').on('click', (e) => {
        $('input[name=Keyword]').val('');
        $('input[name=IsActive][value=""]').prop('checked', true);
        _$employeesTable.ajax.reload();
    });

    $('.txt-search').on('keypress', (e) => {
        if (e.which == 13) {
            _$employeesTable.ajax.reload();
            return false;
        }
    });

})(jQuery);
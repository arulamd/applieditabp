﻿(function ($) {
    //var id = $('#EmployeeEditModal').data('data-employee-id');
    _$modal = $('#CompanyEditModal'),
        _$form = _$modal.find('form');
    var id = $('#company-id').val();

    console.log(id)
    function save() {
        if (!_$form.valid()) {
            return;
        }

        var employee = _$form.serializeFormToObject();

        abp.ui.setBusy(_$form);

        $.ajax({
            url: `/Company/UpdateCompany?${id}`,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(employee),
            success: function (result) {
                _$modal.modal('hide');
                abp.notify.info(l('SavedSuccessfully'));
                abp.event.trigger('tenant.edited', tenant);
            },
            error: function (error) {
                console.error("Update failed:", error);
            }
        });
    }


    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();
    });
})(jQuery);
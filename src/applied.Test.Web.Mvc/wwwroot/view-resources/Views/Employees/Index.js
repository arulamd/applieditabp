﻿(function ($) {    
    //var testUrl = '/Employees/GetEmployee';
    //$.get(testUrl).done(function (testJson) {
    //    console.log(testJson)
    //})

    _$modal = $('#EmployeeCreateModal'),
    _$form = _$modal.find('form'),
    _$table = $('#EmployeeTable');

    var _$employeesTable = _$table.DataTable({
            processing: true, 
            //serverSide: true,
            listAction: {               
                inputFilter: function () {
                    return $('#EmployeesSearchForm').serializeFormToObject(true);
                }
            },
            ajax: {
                "url": '/Employees/GetEmployee', 
                "type": "GET",  
                "contentType": "application/json",
                "datatype": "json", 
                "dataSrc": function (data) {
                    //console.log((data.result));
                    var tableData = data.result;
                    return tableData.data;
                }                           
               
            },
            buttons: [
                {
                    name: 'refresh',
                    text: '<i class="fas fa-redo-alt"></i>',
                    action: () => _$employeesTable.draw(false)
                }
            ],
            responsive: {
                details: {
                    type: 'column'
                }
            },           
            columns: [               
                { "data": "id"},
                { "data": "name" },
                { "data": "description" },
                { "data": "isActived" },               
                { "data": null ,
                    "render": function (data, type, row, meta) {
                        return [
                            `<button type="button" class="btn btn-sm bg-secondary edit-employee" data-employee-id="${row.id}" data-toggle="modal" data-target="#EmployeeEditModal">`,
                                `<i class="fas fa-pencil-alt"></i> Edit`,
                            `</button>`,                           
                            `<button type="button" class="btn btn-sm bg-danger delete-employee" data-employee-id="${row.id}" data-employee-name="${row.name}">`,
                            `    <i class="fas fa-trash"></i> Delete`,
                            `</button>`
                        ].join('');
                    },
                    "orderable": false,
                    "searchable": false
                }
            ]
        //});
    });   
    
    $(document).on('click', '.delete-employee', function () {
        var tenantId = $(this).attr('data-employee-id');
        var tenancyName = $(this).attr('data-employee-name');

        deleteTenant(tenantId, tenancyName);
    });

    $(document).on('click', '.edit-employee', function (e) {
        var tenantId = $(this).attr('data-employee-id');
       
        $.ajax({
            url: '/Employees/EditModal?employeeId=' + tenantId,
            type: 'GET',
            dataType: 'html',
            success: function (content) {
                $('#EmployeeEditModal div.modal-content').html(content);
            },
            error: function (e) {
            }
        });
    });

    //$('#EmployeeCreateModal').on('shown.bs.modal', function () {
    $(document).on("shown.bs.modal", "#EmployeeCreateModal", function () {

        var testUrl = '/Company/GetAllCompanies';
        $.get(testUrl).done(function (testJson) {
            console.log(testJson)
        })

        $.ajax({
            url: '/Company/GetAllCompanies',
            type: 'GET',
            success: function (data) {
                var tableData = data.result;

                var select = $('#CompanyId');
                $.each(tableData, function (i, company) {
                    select.append($('<option>', {
                        value: company.id,
                        text: company.name
                    }));
                });
            },
            error: function () {
                console.error('Failed to fetch companies');
            }
        });
    });

    abp.event.on('employee.edited', (data) => {
        _$employeesTable.ajax.reload();
    });
    
    function deleteTenant(tenantId, tenancyName) {
       
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this data? " + tenancyName,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then(willDelete => {
                if (willDelete) {
                    $.ajax({
                        url: '/Employees/DeleteEmployee?employeeId=' + tenantId,
                        type: 'GET',
                        contentType: 'application/json',                       
                        success: function (result) {
                            _$modal.modal('hide');
                            swal("Delete!", "Your Employee is Delete!", "success"); 
                            //abp.event.trigger('employee.edited', tenancyName);
                            _$employeesTable.ajax.reload();
                        },
                            error: function (error) {
                            console.error("Update failed:", error);
                        }
                    });

                }
                else {
                    swal("Safe!", "Your file is safe!", "success");
                }
            });
    }

    _$modal.on('shown.bs.modal', () => {
        _$modal.find('input:not([type=hidden]):first').focus();
    }).on('hidden.bs.modal', () => {
        _$form.clearForm();
    });

    $('.btn-search').on('click', (e) => {
        _$employeesTable.ajax.reload();
    });

    $('.btn-clear').on('click', (e) => {
        $('input[name=Keyword]').val('');
        $('input[name=IsActive][value=""]').prop('checked', true);
        _$employeesTable.ajax.reload();
    });

    $('.txt-search').on('keypress', (e) => {
        if (e.which == 13) {
            _$employeesTable.ajax.reload();
            return false;
        }
    });

})(jQuery);
﻿(function ($) {
    var testUrl = '/Employees/GetAllCompanies';
    $.get(testUrl).done(function (testJson) {
        console.log(testJson)
    })

    $.ajax({
        url: '/Employees/GetAllCompanies',
        type: 'GET',
        success: function (data) {
            var select = $('#CompanyId');
            $.each(data, function (i, company) {
                select.append($('<option>', {
                    value: company.id,
                    text: company.name
                }));
            });
        },
        error: function () {
            console.error('Failed to fetch companies');
        }
    });
});